﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" Title="Libros"  AutoEventWireup="true"  CodeFile="Libros.aspx.cs" Inherits="Libros" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
	  <div class="row">
      <div class="col-md-4">
      <h3><b>Libros</b></h3>
      
		<p>
			Libro: 
			<asp:TextBox ID="txtLibro" runat="server"></asp:TextBox>
		</p>
		<p>
			Titulo: 
			<asp:TextBox ID="txtTitulo" runat="server" ></asp:TextBox>
		</p>
		<p>
			Autor: 
			<asp:TextBox ID="txtAutor" runat="server"></asp:TextBox>
		</p>
		<p>
			Numero de paginas
			<asp:TextBox ID="txtNumPag" runat="server"></asp:TextBox>
		</p>
			<asp:Button ID="btnEliminar" class="btn btn-danger" runat="server" Text="Eliminar" onclick="btnEliminar_Click"/>
			<asp:Button ID="btnBuscar" class="btn btn-info" runat="server" Text="Buscar" onclick="btnBuscar_Click"/>
			<asp:Button ID="btnCrear" class="btn btn-primary" runat="server" Text="Crear" onclick="btnCrear_Click"/>
            <asp:Button ID="btnActualizar" class="btn btn-default" runat="server" Text="Actualizar" onclick="btnActualizar_Click"/>
		<p>
        </div>
        <div class="col-md-8">
			<asp:GridView ID="GridView1" CssClass="table table-bordered" runat="server"  EnableModelValidation="True">
                <Columns>
            <%--CheckBox para seleccionar varios registros...--%>
            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="70px">
                <ItemTemplate>
                    <asp:CheckBox ID="chk" runat="server" AutoPostBack="true" />
                </ItemTemplate>
            </asp:TemplateField>  
                    </Columns>
			</asp:GridView>
		</div>
    </div>
	</form>
</asp:Content>