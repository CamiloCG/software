﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Prestamos : System.Web.UI.Page
{
    private Conexion con = new Conexion();
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        string consulta = "SELECT p.socio ,p.LIBRO,s.nombre,l.TITULO,p.FPRESTAMO ,p.FDEVOLUCION FROM prestamo p,libro l,socio s where p.LIBRO=l.LIBRO and s.SOCIO=p.SOCIO     ";
        if (txtSocio.Text != "" | txtFDevolucion.Text != "" | txtFPrestamo.Text != "" | txtLibro.Text != "")
        {
            if (txtSocio.Text != "")
            {
                consulta += " socio=" + txtSocio.Text + " and ";
            }
            if (txtFDevolucion.Text != "")
            {
                consulta += " FDevolucion='" + txtFDevolucion.Text + "' and ";
            }
            if (txtFPrestamo.Text != "")
            {
                consulta += " FPrestamo='" + txtFPrestamo.Text + "' and ";
            }
            if (txtLibro.Text != "")
            {
                consulta += " libro=" + txtLibro.Text + "     ";
            }
        }
        //Console.WriteLine(consulta.Substring(0, consulta.Length - 5));
        GridView1 = con.consulta(GridView1, consulta.Substring(0, consulta.Length - 5));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        string consulta = "Delete from prestamo where socio=";

        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkRow = (row.Cells[0].FindControl("chk") as CheckBox);
                if (chkRow.Checked)
                {
                    //Response.Write(consulta + row.Cells[1].Text);
                    con.DML(consulta + row.Cells[1].Text+" and libro="+row.Cells[2].Text);
                }
            }
        }

        btnBuscar_Click(sender, e);
    }

    protected void btnCrear_Click(object sender, EventArgs e)
    {
        if (txtSocio.Text != ""  & txtFPrestamo.Text != "" & txtLibro.Text != "")
        {
            string consulta = "INSERT INTO prestamo (socio,FDevolucion,FPrestamo,libro) VALUES (" + txtSocio.Text + ",'" + txtFDevolucion.Text + "','" + txtFPrestamo.Text + "'," + txtLibro.Text + ")";
            con.DML(consulta);
            btnBuscar_Click(sender, e);
        }
    }

    protected void btnActualizar_Click(object sender, EventArgs e)
    {

        if (txtSocio.Text != "" | txtFDevolucion.Text != "" | txtFPrestamo.Text != "" | txtLibro.Text != "")
        {
            string consulta = "UPDATE prestamo SET ";

            if (txtSocio.Text != "")
            {
                consulta += " socio=" + txtSocio.Text + " ,";
            }
            if (txtFDevolucion.Text != "")
            {
                consulta += " FDevolucion='" + txtFDevolucion.Text + "' ,";
            }
            if (txtFPrestamo.Text != "")
            {
                consulta += " FPrestamo='" + txtFPrestamo.Text + "' ,";
            }
            if (txtLibro.Text != "")
            {
                consulta += " libro=" + txtLibro.Text + "   ";
            }

            foreach (GridViewRow row in GridView1.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chk") as CheckBox);
                    if (chkRow.Checked)
                    {
                        string name = row.Cells[1].Text;
                        Response.Write(consulta.Substring(0, consulta.Length - 1) + " WHERE socio=" + row.Cells[1].Text + " and libro=" + row.Cells[2].Text);
                        con.DML(consulta.Substring(0, consulta.Length - 1) + " WHERE socio=" + row.Cells[1].Text + " and libro=" + row.Cells[2].Text);
                    }
                }
            }
            btnBuscar_Click(sender, e);
        }
    }
}
