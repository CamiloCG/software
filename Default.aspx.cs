﻿using System; 
using System.Collections.Generic; 
using System.Web;
using System.Web.UI; 
using System.Web.UI.WebControls;

//using Oracle.ManagedDataAccess.Client;

//[Guia ASP.NET Por Maestros del web]
public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Conexion con = new Conexion();
        string[] info ={
            "select * from socio",
            "select * from libro",
            "select count(*) as cantidad  from socio",
            "select count(*) as cantidad from libro",
            "select socio.nombre,libro.titulo from prestamo,socio,libro where socio.socio=prestamo.socio and prestamo.libro=libro.libro order by socio.nombre, libro.titulo",
            "select s.nombre ,(select count(*) from prestamo,socio,libro where socio.socio=prestamo.socio and prestamo.libro=libro.libro and socio.socio=s.socio) as total from socio s",
            "select libro.titulo from libro,(select libro from (select libro, count(libro) contador from prestamo group by libro) contar,(select MAX(contador) condicion from (select libro, count(libro) contador from prestamo group by libro)) maximo where contar.contador=maximo.condicion) consulta WHERE libro.libro=consulta.libro",
            "select libro.titulo from libro,(select contar.libro from (select libro, count(libro) contador from prestamo group by libro) contar, (select MIN(contador) minimo from (select libro, count(libro) contador from prestamo group by libro)) minimo where contar.contador=minimo.minimo) condicion where libro.libro=condicion.libro",
            "select libro.titulo from libro where LIBRO in (select l.libro from prestamo p,socio s,libro l where s.socio=p.socio and p.libro=l.libro and p.FDEVOLUCION is not null)",
            "select count(libro.titulo) as total from libro where LIBRO in (select l.libro from prestamo p,socio s,libro l where s.socio=p.socio and p.libro=l.libro)",
            "select count(libro.titulo) as total from libro where LIBRO in (select l.libro from prestamo p,socio s,libro l where s.socio=p.socio and p.libro=l.libro and p.FDEVOLUCION is not null)",
            "select libro.libro from libro where LIBRO in (select l.libro from prestamo p,socio s,libro l where s.socio=p.socio and p.libro=l.libro and p.FDEVOLUCION is null)",
            "select socio.nombre from socio, (select socio from (select socio,count(socio) contador from prestamo group by socio) csocio, (select max(contador) maximo from (select socio,count(socio) contador from prestamo group by socio)) ccontador where csocio.contador=ccontador.maximo) consulta where socio.socio=consulta.socio",
            "select socio.nombre from socio where socio in (select s.socio from prestamo p,socio s,libro l where s.socio=p.socio and p.libro=l.libro and p.FPRESTAMO like '%/03/%')",
            "select count(*) from libro where libro in (select l.libro from prestamo p,socio s,libro l where s.socio=p.socio and p.libro=l.libro and p.FPRESTAMO like '%/01/%')",
            "select l.libro,s.socio,p.FDEVOLUCION-p.FPRESTAMO as dias from prestamo p,socio s,libro l where s.socio=p.socio and p.libro=l.libro and p.FDEVOLUCION is not null",
            "select l.TITULO,s.NOMBRE,trunc(CURRENT_DATE)-p.FPRESTAMO as dias from prestamo p,socio s,libro l where s.socio=p.socio and p.libro=l.libro and p.FDEVOLUCION is null",
            "select libro.titulo, consulta.contar from libro, (select libro, contar from (select libro, count(libro) contar from prestamo group by libro order by contar desc) WHERE rownum<=3) consulta where libro.libro=consulta.libro ORDER BY consulta.contar DESC",
            "select socio.nombre,consulta.contar from socio, (select * from (select socio, count(socio) contar from prestamo group by socio ORDER BY contar DESC) where rownum<=2) consulta where consulta.socio=socio.socio",
            "select l.titulo,s.nombre,p.FDEVOLUCION-p.FPRESTAMO as dias from prestamo p,socio s,libro l where s.socio=p.socio and p.libro=l.libro and p.FDEVOLUCION-p.FPRESTAMO>2 and p.FDEVOLUCION is not null"
        };
        GridView1 = con.consulta(GridView1, info[SelectInfo.SelectedIndex]);
        //onclick="btnInfo_Click"
        //con.DML("UPDATE LIBRO SET NUMPAG=199 WHERE LIBRO=8");
    }

    protected void btnLibros_Click(object sender, EventArgs e)
    {
        Response.Redirect("libro.aspx");
    }
}