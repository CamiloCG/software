﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Socios : System.Web.UI.Page
{
    private Conexion con = new Conexion();
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        string consulta = "select * from socio where";
        if (txtSocio.Text != "" | txtNombre.Text != "" | txtTelefono.Text != "" | txtIngreso.Text != "")
        {
            if (txtSocio.Text != "")
            {
                consulta += " socio=" + txtSocio.Text + " and ";
            }
            if (txtNombre.Text != "")
            {
                consulta += " nombre='" + txtNombre.Text + "' and ";
            }
            if (txtTelefono.Text != "")
            {
                consulta += " telefono=" + txtTelefono.Text + " and ";
            }
            if (txtIngreso.Text != "")
            {
                consulta += " ingreso='" + txtIngreso.Text + "'     ";
            }
        }
        //Console.WriteLine(consulta.Substring(0, consulta.Length - 5));
        GridView1 = con.consulta(GridView1, consulta.Substring(0, consulta.Length - 5));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        string consulta = "Delete from socio where socio=";

        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkRow = (row.Cells[0].FindControl("chk") as CheckBox);
                if (chkRow.Checked)
                {
                    //Response.Write(consulta + row.Cells[1].Text);
                    con.DML(consulta + row.Cells[1].Text);
                }
            }
        }

        btnBuscar_Click(sender, e);
    }

    protected void btnCrear_Click(object sender, EventArgs e)
    {
        if (txtSocio.Text != "" & txtNombre.Text != "" & txtTelefono.Text != "" & txtIngreso.Text != "")
        {
            string consulta = "INSERT INTO socio (socio,nombre,telefono,ingreso) VALUES (" + txtSocio.Text + ",'" + txtNombre.Text + "'," + txtTelefono.Text + ",'" + txtIngreso.Text + "')";
            Response.Write(consulta);
            con.DML(consulta);
            btnBuscar_Click(sender, e);
        }
    }

    protected void btnActualizar_Click(object sender, EventArgs e)
    {

        if (txtSocio.Text != "" | txtNombre.Text != "" | txtTelefono.Text != "" | txtIngreso.Text != "")
        {
            string consulta = "UPDATE socio SET ";

            if (txtSocio.Text != "")
            {
                consulta += " socio=" + txtSocio.Text + " ,";
            }
            if (txtNombre.Text != "")
            {
                consulta += " nombre='" + txtNombre.Text + "' ,";
            }
            if (txtTelefono.Text != "")
            {
                consulta += " telefono=" + txtTelefono.Text + " ,";
            }
            if (txtIngreso.Text != "")
            {
                consulta += " ingreso='" + txtIngreso.Text + "'   ";
            }

            foreach (GridViewRow row in GridView1.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chk") as CheckBox);
                    if (chkRow.Checked)
                    {
                        string name = row.Cells[1].Text;
                        Response.Write(name);
                        con.DML(consulta.Substring(0, consulta.Length - 1) + " WHERE socio=" + row.Cells[1].Text);
                    }
                }
            }
            btnBuscar_Click(sender, e);
        }
    }
}
