﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" Title="Software II"  AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<form id="form1" runat="server">
	<p>
	    <h3><b>Mostrar informes: </b></h3>
			<select class="list-group-item" id="SelectInfo" runat="server">
				<option selected="selected"> Listado de socios y la fecha de ingreso</option>
				<option> Listado de libros</option>
				<option> Cantidad de socios</option>
				<option> Cantidad de libros</option>
				<option> Listado de libros prestados por socio</option>
				<option> Total de libros prestados a cada socio</option>
				<option> Libro mas solicitado</option>
				<option> Libro menos solicitado</option>
				<option> Libros prestados y devueltos</option>
				<option> Total de libros prestados</option>
				<option> Total de libros prestados y devueltos</option>
				<option> Libros prestados y no devueltos</option>
				<option> Socio que mas libros ha solicitado</option>
				<option> Listado de las personas que le fue prestado un libro en el mes de Marzo, mostrar ademas el libro</option>
				<option> Total de libros prestados en el mes de Enero</option>
				<option> Libros prestados y tiempo que demoraron en devolverlo, aplica solo a los libros devueltos</option>
				<option> Listado de libros prestados que no han sido devueltos y el tiempo (en dias) que llevan de estar prestados</option>
				<option> Los tres libros mas solicitados</option>
				<option> Los dos socios que mas solicitan libros</option>
				<option> Listado de socios que han tenido un libro por mas de 2 dias, aplica solo para los libros devueltos</option>
			</select>				
		</p>
		    <asp:Button class="btn btn-info" ID="btnInfo" runat="server" Text="Generar" />
            
		<p>
		<asp:GridView ID="GridView1" CssClass="table table-bordered" runat="server"  EnableModelValidation="True">
	    </asp:GridView>
	</p>
</form>
</asp:Content>