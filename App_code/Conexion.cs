using System;
using System.Collections.Generic;
using System.Web;
using Oracle.ManagedDataAccess.Client;
using System.Configuration;

public class Conexion
{
	//DESKTOP-C5F9C92
	public System.Web.UI.WebControls.GridView consulta(System.Web.UI.WebControls.GridView GridView,string consulta){
	 try 
    	{
            OracleConnection con = ConexionOracle.Conexion;
            con.Open();
            
         OracleCommand cmd = con.CreateCommand();
        cmd.CommandText = consulta;                            
        OracleDataReader reader = cmd.ExecuteReader();
        GridView.DataSource = reader;
        GridView.DataBind();
    	// Close and Dispose OracleConnection
    	con.Close();
		}catch(Exception e){}
		return GridView;
	}

	public void DML(string consulta){
		OracleConnection con = ConexionOracle.Conexion;
        con.Open();
    	OracleCommand command = con.CreateCommand();

        OracleTransaction transaction = con.BeginTransaction();
        command.Transaction = transaction;

        try
        {
            command.CommandText = consulta;
            command.ExecuteNonQuery();
            transaction.Commit();
        }
        catch (Exception e)
        {
            transaction.Rollback();
        }
        con.Close();
	}
}