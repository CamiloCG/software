﻿using System;
using System.Collections.Generic;
using System.Web;
using Oracle.ManagedDataAccess.Client;
using System.Configuration;

public class ConexionOracle
{
    private static OracleConnection conexion=null;
    private static string cadenaconn = "data source=(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = " + ConfigurationManager.AppSettings.Get("host") + ")(PORT = " + ConfigurationManager.AppSettings.Get("port") + "))(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = XE) ));user id=" + ConfigurationManager.AppSettings.Get("user") + ";password=" + ConfigurationManager.AppSettings.Get("pass") + ";Connection Timeout=10;";

    private ConexionOracle() { }

    public static OracleConnection Conexion
    {
        get
        {
            if (conexion == null)
            {
                //DESKTOP-C5F9C92
                conexion = new OracleConnection(cadenaconn);
            }
            return conexion;
        }
    }
}