﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Libros : System.Web.UI.Page
{
    private Conexion con = new Conexion();
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        string consulta = "select * from libro where";
        if (txtLibro.Text != "" | txtTitulo.Text != "" | txtAutor.Text != "" | txtNumPag.Text != "")
        {
            if (txtLibro.Text != "")
            {
                consulta += " libro=" + txtLibro.Text + " and ";
            }
            if (txtTitulo.Text != "")
            {
                consulta += " titulo='" + txtTitulo.Text + "' and ";
            }
            if (txtAutor.Text != "")
            {
                consulta += " autor='" + txtAutor.Text + "' and ";
            }
            if (txtNumPag.Text != "")
            {
                consulta += " numpag=" + txtNumPag.Text + "     ";
            }
        }
        Console.WriteLine(consulta.Substring(0, consulta.Length - 5));
        GridView1 = con.consulta(GridView1, consulta.Substring(0, consulta.Length - 5));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
            string consulta = "Delete from libro where libro=";

            foreach (GridViewRow row in GridView1.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chk") as CheckBox);
                    if (chkRow.Checked)
                    {
                        //Response.Write(consulta + row.Cells[1].Text);
                        con.DML(consulta+ row.Cells[1].Text);
                    }
                }
            }
        
        btnBuscar_Click(sender, e);
    }

    protected void btnCrear_Click(object sender, EventArgs e)
    {
        if (txtLibro.Text != "" & txtTitulo.Text != "" & txtAutor.Text != "" & txtNumPag.Text != "")
        {
            string consulta = "INSERT INTO libro (libro,titulo,autor,numpag) VALUES (" + txtLibro.Text + ",'" + txtTitulo.Text + "','" + txtAutor.Text + "'," + txtNumPag.Text + ")";
            con.DML(consulta);
            btnBuscar_Click(sender, e);
        }
    }

    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        
        if (txtLibro.Text != "" | txtTitulo.Text != "" | txtAutor.Text != "" | txtNumPag.Text != "")
        {
            string consulta = "UPDATE libro SET ";

            if (txtLibro.Text != "")
            {
                consulta += " libro=" + txtLibro.Text + " ,";
            }
            if (txtTitulo.Text != "")
            {
                consulta += " titulo='" + txtTitulo.Text + "' ,";
            }
            if (txtAutor.Text != "")
            {
                consulta += " autor='" + txtAutor.Text + "' ,";
            }
            if (txtNumPag.Text != "")
            {
                consulta += " numpag=" + txtNumPag.Text + "   ";
            }
        
            foreach (GridViewRow row in GridView1.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chk") as CheckBox);
                    if (chkRow.Checked)
                    {
                        string name = row.Cells[1].Text;
                        Response.Write(name);
                        con.DML(consulta.Substring(0, consulta.Length - 1) + " WHERE libro="+ row.Cells[1].Text);
                    }
                }
            }
            btnBuscar_Click(sender, e);
        }
    }
}